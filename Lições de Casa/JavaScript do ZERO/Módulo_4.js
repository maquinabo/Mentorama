//Atividade 1
function tabuada(n1, n2){
    let count1 = 1
    let count2 = n1
    do{
        console.log(`${n1} x ${count1} = ${n1*count1}`)
        count1++
    } while(count1 <= n2)
    console.log(".....Inverso.....")
    do{
        console.log(`${n2} x ${count2} = ${n2*count2}`)
        count2--
    } while(count2 >= 1)
}
tabuada(6,5)
console.log("----------------------------------")

//Atividade 2
function contagem(n){
    let count = 1
    do{
        if(count % 2 == 0){
            console.log(`O número ${count} é Par`)
        }else if(count % 2 !== 0){
            console.log(`O número ${count} é Impar`)
        }if(count % 3 == 0){
            console.log(`O número ${count} é Multiplo de 3`)
        }else if(count % 5 == 0){
            console.log(`O número ${count} é Multiplo de 5`)
        }if(count > 1 && count % count == 0 && count % 1 == 0){
            console.log(`O número ${count} é Primo`)
        }
        console.log("~~~~~~~~~")
        count++
    }while(count < n+1)
}
contagem(5)
console.log("----------------------------------")

//Atividade 3
function Fibonacci(n){
    if (n <= 1){
        return 1
    }
    let soma = Fibonacci(n - 1) + Fibonacci(n - 2)
    return soma
}
let count = 9 
let horizontal = []
for(let i = 0; i <= count; i++){
    horizontal.push(Fibonacci(i))
}
console.log(horizontal)
console.log("----------------------------------")

//Atividade 4
let gabarito = []
function intervalo(n1, n2){
    if(n1 == n2){
        console.log(`Os dois números são iguais a ${n1}!`)
    }
    if(n1 < n2){
    while(n1 < n2){
        gabarito.push(n1)
        n1++
    }
    gabarito.push(n2)
    console.log(gabarito)
}
    else if(n2 < n1){
    while(n2 < n1){
        gabarito.push(n1)
        n1--
    }
    gabarito.push(n1)
    console.log(gabarito)
}
gabarito.splice(0,gabarito.length)
}
intervalo(1,5)
intervalo(5,1)
intervalo(5,5)
