function Embarcacoes(nome, tracao, porte, velocidadeMax, itensResgate){ //itensResgate seriam boias, botes, coletes e etc
    return {
        nome,
        tracao,
        porte,
        velocidadeMax: `${velocidadeMax} km/h`,
        itensResgate,
        seguranca: seguranca(),
        Remos: qtdRemos(),
        BrasilAfricaSul: `${Math.round((7000/velocidadeMax)/24)} dias`
    }

    function seguranca(){
        if(itensResgate > 0){
            return 'Sim'
        }else{
            return 'Não'
        }
    }
    
    function qtdRemos(){
        if(tracao == 'Remo'){
            return Math.ceil(velocidadeMax/2) 
        }else{
            return 0
        }
    }
}

const Balsa = Embarcacoes('Balsa', 'Combustão', 'Grande', 20, 8)
const BarcoVapor = Embarcacoes('Barco a Vapor', 'Vapor', 'Pequeno~Grande', 5, 4)
const BarcoPesca = Embarcacoes('Barco de Pesca', 'Combustão', 'Pequeno~Médio', 10, 0)
const Canoa = Embarcacoes('Canoa', 'Remo', 'Pequeno', 11, 0)
const Cruzeiro = Embarcacoes('Cruzeiro', 'Combustão', 'Gigante', 45, 26)

console.log(Balsa)
console.log(BarcoVapor)
console.log(BarcoPesca)
console.log(Canoa)
console.log(Cruzeiro)