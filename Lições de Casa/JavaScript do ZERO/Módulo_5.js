//Atividade 1
let numeros = [1, 2, 3, 4]
console.log(numeros)

numeros.unshift(0)
console.log(numeros)

numeros.push(5)
console.log(numeros)

numeros.shift()
numeros.pop()
console.log(numeros)

numeros.splice(2,0,50)
console.log(numeros)

//No ultimo item que ele pediu, não entendi muito bem o que ele queria então fiz as duas versões que entendi
//Versão 1
let todosNumeros = numeros.map(n => n)
console.log(todosNumeros)
//Versão 2
let novosNumeros = numeros.map(e => numeros[2]++)
console.log(novosNumeros)
console.log("----------------------------------")

//Atividade 2
let jogados = [4, 2, 5, 1 ,3]
console.log(jogados)

function bubbleCres(array){ //Ordem Crescente
let tamanho = jogados.length

    for(let i = 0; i < tamanho; i++ ){
        for(let j = 0; j < tamanho; j++){
            if(jogados[j] > jogados[j + 1]){
                let temp = jogados[j]
                jogados[j] = jogados [j + 1]
                jogados[j + 1] = temp
            }
        }
    }
    return array
}
console.log(bubbleCres(jogados))

function bubbleDecre(array){ //Ordem Decrescente
    let tamanho = jogados.length
    
        for(let i = 0; i < tamanho; i++ ){
            for(let j = 0; j < tamanho; j++){
                if(jogados[j] < jogados[j + 1]){
                    let temp = jogados[j]
                    jogados[j] = jogados [j + 1]
                    jogados[j + 1] = temp
                }
            }
        }
        return array
    }
console.log(bubbleDecre(jogados))
console.log("----------------------------------")

//Atividade 3
const algoritmos = [1, 3, 4, 31.5, 13.5]
console.log(algoritmos)

const dobro = algoritmos.map(e => e*2)
console.log(dobro)

const par = dobro.filter(e => e % 2 === 0)
console.log(par)

const soma = par.reduce((a, b) => a+b)
console.log(`A soma de todos os itens do Array é ${soma}`)


