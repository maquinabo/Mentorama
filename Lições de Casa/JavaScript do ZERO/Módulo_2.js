// Calcule o dobro e a metade de um número qualquer
function calculo(n){
    return console.log(`O Dobro de ${n} é ${n*2}, e a metade é ${n/2}`)
}
calculo(2)
console.log("----------------------------------")

//Defina duas variáveis que simbolizam o tamanho de 2 lados de um quadrado. Calcule a área desse quadrado
function area(b, h){
    return console.log(`A área do quadrado de base ${b}m e altura ${h}m é de ${b*h}m`)
}
area(8,8)
console.log("----------------------------------")

//Crie uma varável para nome e outra para sobrenome. Imprima na tela o nome completo
function nomeCompleto(p, s){
    return console.log(`Seu nome completo é ${p} ${s}.`)
}
nomeCompleto("Lucas", "Costa")
console.log("----------------------------------")

//Imprima na tela se uma variável é: igual, diferente, maior ou menor que outra variável
function compara(n1, n2){
    let igual = n1 == n2
    let diferente = n1 != n2
    let maior = n1 > n2
    let menor = n1 < n2
    return console.log(`Igual = ${igual}, Diferente = ${diferente}, Maior = ${maior}, Menor = ${menor}`)
}
compara(2,3)
console.log("----------------------------------")

//Faça um conversor de Celsius/Fahrenheit
function conversor(n){
    return console.log(`${n}°C em Fahrenheit é ${n*1.8+32}°F`)
}
conversor(32)
console.log("----------------------------------")

//Faça o cálculo de uma equação de 2 grau (bhaskara)
function Bhaskara(a, b, c){
    let Delta = b**2-4*a*c
    let Positivo = (-b+Math.sqrt(Delta))/2*a
    let Negativo = (-b-Math.sqrt(Delta))/2*a
    let ResultadoP = ""
    let ResultadoN = ""

    if(Delta >= 0){
        console.log(`X' = ${Positivo} ${ResultadoP}`)
        console.log(`X'' = ${Negativo} ${ResultadoN}`)   
    }else{
        console.log("O Delta é Negativo")
    }
}
Bhaskara(1, 3, 2)
Bhaskara(3, 1, 2)
