//Módulo 2
function Bhaskara(a, b, c){
    let Delta = b**2-4*a*c
    let Positivo = (-b+Math.sqrt(Delta))/2*a
    let Negativo = (-b-Math.sqrt(Delta))/2*a
    let ResultadoP = ""
    let ResultadoN = ""

    if(Delta >= 0){
        console.log(`X' = ${Positivo} ${ResultadoP}`)
        console.log(`X'' = ${Negativo} ${ResultadoN}`)   
    }else{
        console.log("O Delta é Negativo")
    }
}
Bhaskara(1, 3, 2)
Bhaskara(3, 1, 2)
console.log("----------------------------------")

//Módulo 3
function diaSeguinte(dia, mes, ano){
    if(dia > 31 || dia < 1 || mes > 12 || mes < 1){//1º condição
        console.log("Essa não é uma data válida!")
    }else if(dia === 31 && mes === 12){//2º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${1}/${1}/${ano+1}.`)
    }else if(dia === 31 && (mes === 1 || mes === 3 || mes === 5 || mes === 7 || mes === 8 || mes === 10 || mes === 12)){//3º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${1}/${mes+1}/${ano}.`)
    }else if(dia === 30 && (mes === 4 || 6 || 9 || 11)){//4º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${1}/${mes+1}/${ano}.`)
    }else if(dia === 31 && (mes === 4 || 6 || 9 || 11)){//5º condição
        console.log("Essa não é uma data válida!")
    }else if(dia === 29 && mes === 2 && ano % 4 === 0){//6º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${1}/${mes+1}/${ano}.`)
    }else if(dia === 29 && mes === 2 && ano % 4 !== 0){//7º condição
        console.log("Essa não é uma data válida!")
    }else if(dia === 28 && mes === 2 && ano % 4 !== 0){//8º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${1}/${mes+1}/${ano}.`)
    }else{//9º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${dia+1}/${mes}/${ano}!.`)
    }
}
diaSeguinte(33,05,2021)//1º condição
diaSeguinte(31,12,2021)//2º condição
diaSeguinte(31,05,2021)//3º condição
diaSeguinte(30,06,2021)//4º condição
diaSeguinte(31,06,2021)//5º condição
diaSeguinte(29,02,2020)//6º condição
diaSeguinte(29,02,2021)//7º condição
diaSeguinte(28,06,2021)//8º condição
diaSeguinte(15,05,2021)//9º condição
console.log("----------------------------------")

//Módulo 4
function Fibonacci(n){
    if (n <= 1){
        return 1
    }
    let soma = Fibonacci(n - 1) + Fibonacci(n - 2)
    return soma
}
let count = 9 
let horizontal = []
for(let i = 0; i <= count; i++){
    horizontal.push(Fibonacci(i))
}
console.log(horizontal)
console.log("----------------------------------")


//Módulo 5
let jogados = [4, 2, 5, 1 ,3]
console.log(jogados)

function bubbleCres(array){ //Ordem Crescente
let tamanho = jogados.length

    for(let i = 0; i < tamanho; i++ ){
        for(let j = 0; j < tamanho; j++){
            if(jogados[j] > jogados[j + 1]){
                let temp = jogados[j]
                jogados[j] = jogados [j + 1]
                jogados[j + 1] = temp
            }
        }
    }
    return array
}
console.log(bubbleCres(jogados))

function bubbleDecre(array){ //Ordem Decrescente
    let tamanho = jogados.length
    
        for(let i = 0; i < tamanho; i++ ){
            for(let j = 0; j < tamanho; j++){
                if(jogados[j] < jogados[j + 1]){
                    let temp = jogados[j]
                    jogados[j] = jogados [j + 1]
                    jogados[j + 1] = temp
                }
            }
        }
        return array
    }
console.log(bubbleDecre(jogados))
