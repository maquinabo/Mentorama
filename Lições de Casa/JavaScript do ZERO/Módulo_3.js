//Atividade 1
function prestacao(valorT, prest){
    if(prest < 12){
        console.log("O número mínimo de prestações é 12")
    }else if(prest >= 24 && prest < 36){
        console.log(`Cada prestação será ${((valorT*0.1+valorT)/prest).toFixed(2)}`)
    }else if(prest >= 36 ){
        console.log(`Cada prestação será ${((valorT*0.15+valorT)/prest).toFixed(2)}`)
    }else{
        console.log(`Cada prestação será ${(valorT/prest).toFixed(2)}`)
    } 
}
prestacao(1000,11)
prestacao(1000,12)
prestacao(1000,24)
prestacao(1000,36)
console.log("----------------------------------")

//Atividade 2
function IMC(peso, altura){
    let imc = (peso/altura**2).toFixed(2)
    if(imc < 18.5){
        console.log(`Seu IMC é de ${imc} e você está abaixo do peso`)
    }else if(imc >= 18.5 && imc <= 25){
        console.log(`Seu IMC é de ${imc} e você está com o peso normal.`)
    }else if(imc >= 25 && imc <= 30){
        console.log(`Seu IMC é de ${imc} e você está acima do peso.`)
    }else if(imc > 30){
        console.log(`Seu IMC é de ${imc} e você está obeso.`)
    }
}
IMC(50,1.72)
IMC(55,1.72)
IMC(75,1.72)
IMC(90,1.72)
console.log("----------------------------------")

//Atividade 3
function triangulo(a , b , c){
    if (isNaN(a) || isNaN(b) || isNaN(c)){
        console.log('Não é um Triângulo.')
    }else if(a == b || a == c || b == c){
        console.log('O Triângulo é Isósceles.')
    }else if(a !== b && b !== c){
        console.log('O Triângulo é Escaleno.')
    }else if(a == b && b == c){
        console.log('O Triângulo é Equilátero.') 
    }
}
triangulo(2,2,2)
triangulo(2,2,3)
triangulo(2,3,4)
triangulo(2,3,"Não sou um número")
console.log("----------------------------------")

//Atividade 4
let Brasil = {
    nome: "Brasil",
    temper: "°C"
}
let EUA = {
    nome: "EUA",
    temper: "°F"
}

function conversor(temperatura, pais){
    if(pais == "°C"){
        console.log(`O ${Brasil.nome} está ${temperatura*1.8+32}°F`)
    }else if(pais == "°F"){
        console.log(`O ${EUA.nome} está ${(temperatura-32)*5/9}°C`)
    }
}
conversor(25,Brasil.temper)
conversor(86,EUA.temper)
console.log("----------------------------------")

//Atividade 5
function diaSeguinte(dia, mes, ano){
    if(dia > 31 || dia < 1 || mes > 12 || mes < 1){//1º condição
        console.log("Essa não é uma data válida!")
    }else if(dia === 31 && mes === 12){//2º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${1}/${1}/${ano+1}.`)
    }else if(dia === 31 && (mes === 1 || mes === 3 || mes === 5 || mes === 7 || mes === 8 || mes === 10 || mes === 12)){//3º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${1}/${mes+1}/${ano}.`)
    }else if(dia === 30 && (mes === 4 || 6 || 9 || 11)){//4º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${1}/${mes+1}/${ano}.`)
    }else if(dia === 31 && (mes === 4 || 6 || 9 || 11)){//5º condição
        console.log("Essa não é uma data válida!")
    }else if(dia === 29 && mes === 2 && ano % 4 === 0){//6º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${1}/${mes+1}/${ano}.`)
    }else if(dia === 29 && mes === 2 && ano % 4 !== 0){//7º condição
        console.log("Essa não é uma data válida!")
    }else if(dia === 28 && mes === 2 && ano % 4 !== 0){//8º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${1}/${mes+1}/${ano}.`)
    }else{//9º condição
        console.log(`Hoje é ${dia}/${mes}/${ano}, e amanhã será ${dia+1}/${mes}/${ano}!.`)
    }
}
diaSeguinte(33,05,2021)//1º condição
diaSeguinte(31,12,2021)//2º condição
diaSeguinte(31,05,2021)//3º condição
diaSeguinte(30,06,2021)//4º condição
diaSeguinte(31,06,2021)//5º condição
diaSeguinte(29,02,2020)//6º condição
diaSeguinte(29,02,2021)//7º condição
diaSeguinte(28,06,2021)//8º condição
diaSeguinte(15,05,2021)//9º condição
